# WE-Module3

This Gitlab Repo contains the assignments to be completed for the generative AI course of module 3.
* Assignment 1: Computational Thinking
* Assignment 2: Game of Yahtzee
* Assignment 3: Yahtzee Scorer Code
* Assignemnt 4: Markov chains
* Assignemnt 5: Diamonds Bidding Card Game